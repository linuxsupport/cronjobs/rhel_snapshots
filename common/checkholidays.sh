#!/bin/bash

source /root/common.sh

MMDD=`/bin/date +%m%d --date="$TODAY" | sed 's/^0//'` # remove leading zero so the number is not interpreted as octal
WEEKDAY=`/bin/date +%u --date="$TODAY"`

# If we're being forced to move symlinks, holidays don't matter
[[ -f "$DESTINATION/.forcemove.${RELEASE}" ]] && exit 0

# Check if it's Christmas (16th Dec - 6th Jan)
if [[ $MMDD -ge 1216 ]] || [[ $MMDD -le 106 ]]; then
  echo "'Tis the season to not update symlinks"
  exit 1
fi

# Check if it's a weekend
if [[ $WEEKDAY -gt 5 ]]; then
  echo "Thou shalt not linketh on the Sabbath or the Lord's Day"
  exit 2
fi
