#!/usr/bin/python3

import sys
import csv
import re
from Levenshtein import distance
import rpm
from functools import cmp_to_key


# This script takes a CSV file with the following fields:
FIELD_NAMES = ['rpm', 'name', 'version', 'release', 'arch', 'srpm', 'buildtime']

packages = []
try:
    with open(sys.argv[1], 'r') if len(sys.argv) > 1 and sys.argv[1] != "-" else sys.stdin as f:
        reader = csv.DictReader(f, fieldnames=FIELD_NAMES, delimiter=';')
        packages = list(reader)
except:
    sys.exit(1)

if not packages:
    # Nothing to do, we can stop here
    sys.exit(0)


### Now we're going to start filtering the list of packages

## LOS-483 Do not include i686 packages
packages = filter(lambda p: p['arch'] != 'i686', packages)

## Remove debuginfo and debugsource packages
debug = re.compile(r'debug(info|source)$')
packages = filter(lambda p: not debug.search(p['name']), packages)

## Docs packages are boring, drop them too
docs = re.compile(r'-(docs|man-pages|javadoc)')
packages = filter(lambda p: not docs.search(p['name']), packages)

## Deduplicate the list of packages and keep just the "main one" per srpm
# The "main one" is identified by the smallest Levenshtein distance between the
# package name and the srpm name.
# First we'll group the packages by srpm
sources = {}
for p in packages:
    if p['srpm'] not in sources:
        sources[p['srpm']] = []

    sources[p['srpm']].append(p)

# Then we'll sort the packages by distance to find the main one
for s in sources:
    sources[s] = sorted(sources[s], key=lambda p: distance(p['rpm'], s))

# Finally, we'll keep just the main one for each srpm
packages = [sources[s][0] for s in sources]

## Sort the list of packages by name and version

def pkg_sort(p1, p2):
    # First, sort by name
    if p1['name'].lower() < p2['name'].lower():
        return -1
    elif p1['name'].lower() > p2['name'].lower():
        return 1
    else:
        # If the names are the same, sort by buildtime (if we have it), newest first
        if p1['buildtime'] and p2['buildtime']:
            return -1*(int(p1['buildtime']) - int(p2['buildtime']))
        else:
            # and if we don't have a buildtime, fall back to the version comparison
            return not rpm.labelCompare(('1', p1['version'], p1['release']), ('1', p2['version'], p2['release']))

packages = sorted(packages, key=cmp_to_key(pkg_sort))

## Remove duplicate versions of a given package name, keeping the first one (ie. the newest)
seen = set()
packages = [p for p in packages if not p['name'] in seen and not seen.add(p['name'])]

## Write the output as a CSV file
writer = csv.DictWriter(sys.stdout, fieldnames=FIELD_NAMES, delimiter=';')
writer.writerows(packages)
