## Basic config file
LONG_RELEASE="rhel${RELEASE}"
FULL_OS_NAME="Red Hat Enterprise Linux ${RELEASE}"
RELEASE_RPM="redhat-release"
KOJI_TAG_SUFFIX="${RELEASE}el-stable"
TODAY=`/bin/date +%Y%m%d`
TODAY_FORMAT=`/bin/date +%Y-%m-%d`
YESTERDAY=`/bin/date +%Y%m%d --date='yesterday'`
DELAYED=`/bin/date +%Y%m%d --date="$PRODDATE"`
TOO_OLD=`/bin/date +%Y%m%d --date="$OLDESTDATE"`

SOURCE="/data/cdn.redhat.com/content/dist/rhel${RELEASE}/${RELEASE}"
IMAGE_SOURCE="/data/enterprise/rhel/server/${RELEASE}/latest"
DESTINATION="/data/$PATH_DESTINATION"
KOJI_RPMS="/data/internal/repos"
SNAPS_DIR="${RELEASE}-snapshots"
SNAPS="$DESTINATION/$SNAPS_DIR"
DEST=$SNAPS/.tmp.$TODAY
TODELETE=$SNAPS/.todelete.$TODAY
FINALDEST=$SNAPS/$TODAY

UPSTREAM_REPOS="baseos appstream highavailability rt codeready-builder" # resilientstorage supplementary
KOJI_REPOS="cern${KOJI_TAG_SUFFIX} openafs${KOJI_TAG_SUFFIX}"
SUPPORTED_ARCHES="x86_64 aarch64"
ALL_REPOS="CERN openafs $UPSTREAM_REPOS"

LN_TESTING="testing"
WEBSITE="https://linux.cern.ch"
MAILMX="cernmx.cern.ch"

SHORTLIST_COUNT=4
MAX_PACKAGES_LIST=25
DANGEROUS_RPMS="$RELEASE_RPM epel-release"
HIGHLIGHT_RPMS="$RELEASE_RPM kernel kernel-plus kernel-rt dbus glibc \
  cern-get-keytab hepix qemu-kvm systemd libvirt firewalld python sssd kmod-openafs"

ADVISORY_DB="${LONG_RELEASE^^}"
