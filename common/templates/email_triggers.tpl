To: $EMAIL_ADMIN
From: $EMAIL_FROM
Reply-To: noreply.$EMAIL_FROM
Return-Path: $EMAIL_ADMIN
Subject: =?utf-8?Q?[$RELEASE_ACRONYM]: Failed trigger: automation stopped =F0=9F=9B=91?=

Dear admins,

********************************
***  Manual action required  ***
********************************

The following failed trigger has been detected in today's snapshot:

$TRIGGER_MSG

A .freeze.${RELEASE}all file has been created and links will not be updated.

---
Best regards,
CERN Linux Droid
(on behalf of the friendly humans of Linux Support)
