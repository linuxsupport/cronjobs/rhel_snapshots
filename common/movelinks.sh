#!/bin/bash

source /root/common.sh
source /root/common_functions.sh

getLinkDate() {
  readlink "$1" | sed "s/$SNAPS_DIR\///"
}

isLinkNotFrozen() {
  ! ([ -f "$DESTINATION/.freeze.${RELEASE}all" ] || [ -f "$DESTINATION/.freeze.$1" ])
}

setLink() {
  TARGET=$1
  SOURCE=$2
  TARGET_DATE=`echo $TARGET | sed "s/$SNAPS_DIR\///"`
  SOURCE_DATE=`getLinkDate $SOURCE`

  if isLinkNotFrozen $SOURCE && [[ $TARGET_DATE -gt $SOURCE_DATE ]]; then
    echo -n " Link "
    linkTargetTo $TARGET $SOURCE

    # Aggregate all the diffs between $SOURCE_DATE and $TARGET_DATE
    DATE=`addDays $SOURCE_DATE`
    while [[ $DATE -le $TARGET_DATE ]]; do
      for REPO in $ALL_REPOS; do
        CLEANREPO="${REPO/\//-}"
        for ARCH in $SUPPORTED_ARCHES; do
          DIFF=`find "$SNAPS/$DATE/$REPO/$ARCH/" -name .diff`
          cat "$TMPDIR/.diff:${SOURCE}:${REPO/\//-}:${ARCH}" $DIFF 2>/dev/null | sort -u -o "$TMPDIR/.diff:${SOURCE}:${REPO/\//-}:${ARCH}"
        done

        # If there's no diff, just delete the file
        [ -s "$TMPDIR/.diff:${SOURCE}:${REPO/\//-}:${ARCH}" ] || rm -f "$TMPDIR/.diff:${SOURCE}:${REPO/\//-}:${ARCH}"
      done

      # If this date includes a new release, make a note of it
      [ -f "$SNAPS/$DATE/.new-release" ] && cat "$SNAPS/$DATE/.new-release" > "$TMPDIR/.${SOURCE}-new-release"

      DATE=`addDays $DATE`
    done

    return 0
  fi
  return 1
}


cd $DESTINATION

if [ -f "$DESTINATION/.freeze.${RELEASE}all" ]; then
  # A .freeze.*all file exists, so validation or triggers failed
  # Let's make sure today's snapshot exists, even if it's as a symlink to yesterday
  cd $SNAPS
  linkTargetTo $YESTERDAY $TODAY

  echo ".freeze.${RELEASE}all exists, validation or trigger error. Not updating links."
  exit
fi

# No .freeze.*all file, so all went well
# Move $TODAY snapshot to a directory to be later deleted
mv -v $FINALDEST $TODELETE 2>/dev/null
# Promote .tmp.$TODAY to $TODAY
mv -v $DEST $FINALDEST
# Relink .*-latest to today's snapshot
linkTargetTo $SNAPS_DIR/$TODAY ".${RELEASE}-latest"

# Don't update links on holidays
/root/checkholidays.sh || exit

echo "Checking symlinks"

if [[ -f "$DESTINATION/.forcemove.${RELEASE}" || -f "$DESTINATION/.forcemove.${TODAY}.${RELEASE}" ]]; then
  # If we have both a regular forcemove and a scheduled one, ignore the scheduled one
  [[ -f "$DESTINATION/.forcemove.${RELEASE}" && -f "$DESTINATION/.forcemove.${TODAY}.${RELEASE}" ]] && rm -fv "$DESTINATION/.forcemove.${TODAY}.${RELEASE}"
  # We have a scheduled forcemove file for today, so let's use it
  [[ -f "$DESTINATION/.forcemove.${TODAY}.${RELEASE}" ]] && mv -v "$DESTINATION/.forcemove.${TODAY}.${RELEASE}" "$DESTINATION/.forcemove.${RELEASE}"

  # We're being forced to move the production symlinks, accelerating the
  # deployment to production. Let's do some sanity checks first.
  FORCED=`cat "$DESTINATION/.forcemove.${RELEASE}"`
  echo "Found .forcemove.${RELEASE}, should move production symlinks to $SNAPS_DIR/$FORCED"
  if [[ ! -d "$SNAPS_DIR/$FORCED" ]]; then
    echo "That path does not exist or is not a directory, won't do it!"
    exit 1
  fi
  echo "Ok, directory exists, production symlinks will point to $FORCED"
  # Note that symlinks are *never* moved backwards in time, no matter
  # what the .forcemove.* file says. setLink() enforces this.
  DELAYED=$FORCED
  rm "$DESTINATION/.forcemove.${RELEASE}"
fi

TMPDIR=`mktemp -d`

# Let's check what today's point release is (checking just the first arch is enough)
TODAY_POINT_RELEASE=$(repoquery --refresh --repoid baseos_release --repofrompath=baseos_release,$SNAPS_DIR/$TODAY/baseos/${SUPPORTED_ARCHES%% *}/os/ --qf="%{version}" --latest-limit 1 $RELEASE_RPM 2>/dev/null)
# Find the last point release symlink we have
LAST_POINT_RELEASE=$(find . -maxdepth 1 -type l -name "${RELEASE}.*" -printf '%f\n' | sort -V | tail -n1)

if [[ "$TODAY_POINT_RELEASE" != "$LAST_POINT_RELEASE" ]]; then
  # So, today we have a new point release!
  # Record the new point release so we can automatically download the release notes later
  echo "$TODAY_POINT_RELEASE" > "$DESTINATION/.${RELEASE}-latest-release"
# And also so we can announce it properly
  echo "$TODAY_POINT_RELEASE" > "$SNAPS_DIR/$TODAY/.new-release"

  # Let's make a new symlink for that point release
  setLink $SNAPS_DIR/$TODAY "$TODAY_POINT_RELEASE"
  # Upload the new point release to AIMS
  /root/aims.sh $SNAPS_DIR/$TODAY $TODAY "point" "$TODAY_POINT_RELEASE"
  # Now trigger Config's Rundeck job to define the new point release in Foreman
  #/root/rundeck.sh "$TODAY_POINT_RELEASE"
fi

# *-testing is always today, * is always delayed
setLink $SNAPS_DIR/$TODAY "$RELEASE-$LN_TESTING"
SHORTEST_TEST=$?
# Only move the prod symlink on Wednesdays or if we're being forced to
if [[ $(date +%u) -eq 3 || -n "$FORCED" ]]; then
  setLink $SNAPS_DIR/$DELAYED "$RELEASE"
  SHORTEST_PROD=$?
fi

if [[ $SHORTEST_TEST == 0 ]]; then
  /root/regen-repos.sh "${LONG_RELEASE}-${LN_TESTING}-.*"
  /root/sendemail.sh $TMPDIR $LN_TESTING
  /root/aims.sh $SNAPS_DIR/$TODAY $TODAY $LN_TESTING
fi

if [[ $SHORTEST_PROD == 0 ]]; then
  /root/regen-repos.sh "${LONG_RELEASE}-.*" ".*${RELEASE}el-build"
  /root/sendemail.sh $TMPDIR
  /root/aims.sh $SNAPS_DIR/$DELAYED $DELAYED
fi
