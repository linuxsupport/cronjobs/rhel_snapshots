# RHEL snapshots

This repo is the source for rhel{8,9}_snapshots.

Files common to both jobs go in the `common/` subdirectory, but they can be overridden
for each job by placing files in the `rhel8/` or `rhel9/` subdirectories.
