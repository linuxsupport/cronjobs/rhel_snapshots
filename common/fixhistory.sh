#!/bin/bash

source /root/common.sh
source /root/common_functions.sh

mkdir -pv $SNAPS
cd $SNAPS
COUNTER=0

# Let's find the most recent snapshot that exists
PREV=$YESTERDAY
while [[ ! -e "$PREV" && $COUNTER -lt 30 ]]; do
    PREV=`addDays $PREV -1`
    COUNTER=$((COUNTER + 1))
done

# Now we link from that snapshot back to today. We do this
# in this order so linkTargetTo can figure out the final non-symlink target
NEXT=`addDays $PREV 1`
while [[ "$NEXT" != "$TODAY" ]]; do
    echo "Snapshot for $NEXT doesn't exist, linking to $PREV"
    linkTargetTo $PREV $NEXT
    PREV=`addDays $PREV 1`
    NEXT=`addDays $PREV 1`
done
