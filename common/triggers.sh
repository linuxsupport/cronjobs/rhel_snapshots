#!/bin/bash

source /root/common.sh
source /root/common_functions.sh

# Add here any other trigger that we might need

# Trigger 1; LOS-473: Trigger OpenAFS kmod builds on kernel updates
# -----------------------------------------------------------------

DIST=rh${RELEASE}.cern
TRIGGER_REPO="linuxsupport/rpms/openafs"
# Retrieve GitLab CI token for the linuxci service account
LINUXCI_API_TOKEN=$(cat /secrets/linuxci_api_token)

fail() {
  # Something went wrong, freeze and email
  touch -a "$DESTINATION/.freeze.${RELEASE}all"
  template email_triggers.tpl TRIGGER_MSG="$1" | swaks --server $MAILMX --to $EMAIL_ADMIN --data -
  exit 0
}

waitFor() {
  MAX_ATTEMPTS=$1
  shift
  POLLING_INTERVAL=$1
  shift
  ATTEMPT_COUNTER=0

  while [[ $ATTEMPT_COUNTER -lt $MAX_ATTEMPTS ]]; do
    ATTEMPT_COUNTER=$(($ATTEMPT_COUNTER+1))
    # Run the user's command
    $@
    RET=$?

    case $RET in
      0)
        # 0 for success
        return 0
        ;;
      2)
        # 2 for canceled
        return 2
        ;;
      3)
        # 3 for failed
        return 3
        ;;
      1|4)
        # 1 for unknown state, 4 for pending/still running
        ;;
    esac

    sleep $POLLING_INTERVAL
  done

  echo "Max attempts reached."
  # 1 for timeout
  return 1
}

checkPipeline() {
  # Use https://docs.gitlab.com/ee/api/README.html#namespaced-path-encoding
  PROJECT=$1
  PROJECT_PATH_ENC=$(echo $PROJECT | sed 's#/#%2F#g' )
  PIPELINE_ID=$2

  # Wait for pipeline to finish
  POLLING_STATUS="$(curl -sL --max-time 10 --header "PRIVATE-TOKEN: ${LINUXCI_API_TOKEN}" \
    https://gitlab.cern.ch/api/v4/projects/${PROJECT_PATH_ENC}/pipelines/${PIPELINE_ID} | jq -r .status)"

  case $POLLING_STATUS in
    "created"|"running"|"pending")
      echo "Still running, check https://gitlab.cern.ch/${PROJECT}/pipelines/${PIPELINE_ID}"
      return 4
      ;;
    "success")
      return 0
      ;;
    "canceled")
      return 2
      ;;
    "failed")
      return 3
      ;;
    *)
      echo "Unknown state: ${POLLING_STATUS}."
      return 1
      ;;
  esac
}

checkRepo() {
  local REPO=$1
  local PACKAGE=$2

  local SEARCH=$(repoquery --refresh --repoid search_repo --repofrompath=search_repo,$REPO $PACKAGE 2>/dev/null | wc -l)
  [ $SEARCH -gt 0 ] && return 0
  return 4
}


# Find the newest openafs version
AFS_VER=$(repoquery --refresh --repoid openafs_kmod --repofrompath=openafs_kmod,$DEST/openafs/x86_64/ --qf="%{version}-%{release}" --latest-limit 1 openafs 2>/dev/null)
# Remove $DIST from the end of the version
AFS_VER="${AFS_VER%.$DIST}"

# Find the newest kernel-core. Note that here version and release are separated by an _,
# which is what build_kmod.sh in $TRIGGER_REPO does
KERNEL_VER=$(repoquery --refresh --repoid baseos_kmod --repofrompath=baseos_kmod,$DEST/baseos/x86_64/os/ --qf="%{version}_%{release}" --latest-limit 1 kernel-core 2>/dev/null)

# Set up the repo url where https://gitlab.cern.ch/linuxsupport/rpms/openafs/-/blob/master/build_kmod.sh should look for the KERNEL_VER
SNAPSHOT_PATH="${SNAPS_DIR}/.tmp.${TODAY}/"

# Given those AFS and kernel versions, what kmod should we have?
EXPECTED_KMOD="${AFS_VER}.${KERNEL_VER}.${DIST}"

# See if the version we expect is already in the repo
set -x
if checkRepo "$DEST/openafs/x86_64/" "kmod-openafs-${EXPECTED_KMOD}"; then
  echo "No need to rebuild anything, kmod-openafs is already up-to-date."
  exit 0
fi
set +x


# The kmod does not exist, so we need to trigger a pipeline for the specified ref with rebuilding variables, using linuxci as the user
# Use https://docs.gitlab.com/ee/api/README.html#namespaced-path-encoding, WATCH OUT IF MOVING THE PROJECT
# Tell the pipeline the KVER we want, just in case:
KVER=$(repoquery --refresh --repoid baseos_kmod --repofrompath=baseos_kmod,$DEST/baseos/x86_64/os/ --qf="%{version}-%{release}" --latest-limit 1 kernel-core 2>/dev/null)
for J in BUILD_9al BUILD_9el BUILD_8al BUILD_8el BUILD_7; do
  DISABLED="${DISABLED},{\"key\": \"${J}\", \"value\": \"False\"}"
done
PIPELINE_ID=$(curl -sL --request POST \
  --header "PRIVATE-TOKEN: $LINUXCI_API_TOKEN" \
  --header "Content-Type: application/json" \
    --data "{ \"ref\": \"master\", \"variables\": [ {\"key\": \"BUILD_KMOD${RELEASE}el\", \"value\": \"True\"}, {\"key\": \"KVER\", \"value\": \"$KVER\"}, {\"key\": \"SNAPSHOT_PATH\", \"value\": \"$SNAPSHOT_PATH\"} $DISABLED ] }" \
  "https://gitlab.cern.ch/api/v4/projects/linuxsupport%2Frpms%2Fopenafs/pipeline" | jq -r .id)

echo "Triggering kmod openafs rebuild for ${EXPECTED_KMOD} on https://gitlab.cern.ch/${TRIGGER_REPO}/pipelines/${PIPELINE_ID}"

if [ "$PIPELINE_ID" == "null" ]; then
  echo "Could not create a pipeline for https://gitlab.cern.ch/${TRIGGER_REPO}, creating freeze"
  fail "Trigger OpenAFS kmod builds on kernel updates, pipeline creation failed: https://gitlab.cern.ch/${TRIGGER_REPO}/pipelines/"
fi

# 360 attempts and 60 sec poll interval equals to 6 hours.
# Usual pipelines are about 4 hours long: https://gitlab.cern.ch/linuxsupport/rpms/openafs/pipelines
# Adding 2 extra hours for possible waiting times in CI and Koji
waitFor 360 60 checkPipeline "linuxsupport/rpms/openafs" $PIPELINE_ID
RETURN_CODE=$?
if [ $RETURN_CODE -ne 0 ]; then
  case $RETURN_CODE in
    1)
      echo "Pipeline https://gitlab.cern.ch/${TRIGGER_REPO}/pipelines/${PIPELINE_ID} reached the timeout, creating freeze"
      fail "Trigger OpenAFS kmod builds on kernel updates, pipeline reached the timeout: https://gitlab.cern.ch/${TRIGGER_REPO}/pipelines/${PIPELINE_ID}"
      ;;
    2)
      echo "Pipeline https://gitlab.cern.ch/${TRIGGER_REPO}/pipelines/${PIPELINE_ID} was canceled, creating freeze"
      fail "Trigger OpenAFS kmod builds on kernel updates, pipeline was canceled: https://gitlab.cern.ch/${TRIGGER_REPO}/pipelines/${PIPELINE_ID}"
      ;;
    3)
      echo "Pipeline https://gitlab.cern.ch/${TRIGGER_REPO}/pipelines/${PIPELINE_ID} was failed, creating freeze"
      fail "Trigger OpenAFS kmod builds on kernel updates, pipeline failed: https://gitlab.cern.ch/${TRIGGER_REPO}/pipelines/${PIPELINE_ID}"
      ;;
    4)
      echo "Pipeline https://gitlab.cern.ch/${TRIGGER_REPO}/pipelines/${PIPELINE_ID} got an unknown status, creating freeze"
      fail "Trigger OpenAFS kmod builds on kernel updates, pipeline has unknown status: https://gitlab.cern.ch/${TRIGGER_REPO}/pipelines/${PIPELINE_ID}"
      ;;
  esac
fi

echo "Pipeline https://gitlab.cern.ch/${TRIGGER_REPO}/pipelines/${PIPELINE_ID} succeeded, resync the snapshot"

set -x
for ARCH in $SUPPORTED_ARCHES; do
  KOJI_REPO="http://linuxsoft.cern.ch/internal/repos/openafs${KOJI_TAG_SUFFIX}/${ARCH}/os"
  # 360 attempts every 10 seconds is 1 hour. Overly cautious, should be done in less than 30 seconds.
  waitFor 360 10 checkRepo "${KOJI_REPO}" "kmod-openafs-${EXPECTED_KMOD}"
  if [ $? -ne 0 ]; then
    echo "Can't find the new RPM kmod-openafs-${EXPECTED_KMOD} in the repository at: ${KOJI_REPO}"
    fail "Trigger OpenAFS kmod builds on kernel updates, can't find new RPM kmod-openafs-${EXPECTED_KMOD} at: ${KOJI_REPO}"
  fi
done
set +x

# Our pipeline finished, built the new kmod-openafs and Mash has synced it to where it's supposed to be.
# Now we just need to redo the openafs part of today's snapshot to get the new RPMs.
rm -rf $DEST/openafs
snapshotKojiRepo "openafs${KOJI_TAG_SUFFIX}"

echo "New kmod-openafs in place, proceed with the snapshoting scripts."
