#!/bin/bash

source /root/common.sh

POINT=$1

# Rather than introduce a new variable, test if $DESTINATION is not prod
if [ "$DESTINATION" != "/data/cern/rhel" ]; then
  exit
fi

TOKEN=$(cat /secrets/rundeck_api_token)

echo "Triggering Rundeck job..."
OUTPUT=$(curl -X POST --silent --fail --show-error \
    https://joeight.cern.ch:8443/api/18/job/cbb3bd23-ce6a-4a45-b3b2-88f053575365/run \
    -H "X-Rundeck-Auth-Token: ${TOKEN}" \
    -H "Accept: application/json" \
    -H "Content-Type: application/json" \
    --data "{\"options\": {\"major\":\"${RELEASE}\", \"minor\":\"${POINT}\"}}" \
    2>&1
    )
RET=$?
if [[ $RET -ne 0 ]]; then
  echo "Error triggering Rundeck job"
  echo "${OUTPUT}"
  exit 1
fi

PERMALINK=$(echo $OUTPUT | jq -r .permalink)

echo "Job submitted: ${PERMALINK}"

# Make the output slightly readable, but as a single line so Kibana doesn't screw it up
echo "Full output: $(echo $OUTPUT | jq . | tr -d '\n')"
