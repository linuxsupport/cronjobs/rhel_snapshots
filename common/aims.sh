#!/bin/bash

source /root/common.sh

TMPDIR=$1
DATE=$2
TESTING=$3
POINT=$4


[ -n "$POINT" ] && POINT="_${POINT^^}"

cat /secrets/linuxci_pw | kinit linuxci

for ARCH in $SUPPORTED_ARCHES; do
  NAME="${LONG_RELEASE^^}${POINT}_${ARCH^^}"
  DESC="${FULL_OS_NAME^^} (${DATE}) ${ARCH^^}"
  PATH="${TMPDIR}/baseos/${ARCH}/os/images/pxeboot"

  if [[ $TESTING == $LN_TESTING ]]; then
    NAME="${NAME}_TEST"
    DESC="${DESC} TEST"
  fi

  echo "Uploading image to AIMSTEST: ${NAME} - ${DESC}"
  /usr/bin/aims2client addimg \
    --testserver \
    --name "${NAME}" \
    --description "${DESC}" \
    --arch "${ARCH}" \
    --vmlinuz "${PATH}/vmlinuz" \
    --initrd "${PATH}/initrd.img" \
    --egroup "${EMAIL_ADMIN}" \
    --noprogress \
    --force \
    --uefi

  # Rather than introduce a new variable, test if $DESTINATION is prod
  # Only upload to production AIMS if we're in prod
  if [ "$DESTINATION" == "/data/cern/rhel" ]; then
    echo "Uploading image to AIMS: ${NAME} - ${DESC}"
    /usr/bin/aims2client ${TEST} addimg \
      --name "${NAME}" \
      --description "${DESC}" \
      --arch "${ARCH}" \
      --vmlinuz "${PATH}/vmlinuz" \
      --initrd "${PATH}/initrd.img" \
      --egroup "${EMAIL_ADMIN}" \
      --noprogress \
      --force \
      --uefi
  fi
done
