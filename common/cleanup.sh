#!/bin/bash
#
# find all dirs older than $TOO_OLD
# exclude any with symlinks pointing to them
# delete the rest

source /root/common.sh
source /root/common_functions.sh

echo "Deleting snapshots older than $TOO_OLD"

# Delete old $TODAY snapshot, if there is one
quickDelete $TODELETE

cd $SNAPS
CANDIDATES=`find . -maxdepth 1 -type d -or -type l | grep -v "\.\$" | sed 's/\.\///' | sort | awk "{if(\\$1==\\$1+0 && \\$1<$TOO_OLD)print \\$1}" | sort -r`

for f in $CANDIDATES; do
  has_link=`find $DESTINATION -maxdepth 2 -lname "*/$f" -or -lname "$f" | wc -l`
  if [ $has_link -eq 0 ]; then
    echo " -> deleting $f ..."
    quickDelete $SNAPS/$f
  fi
done

# Help Ceph heal after (possibly) deleting primary copies of files
# by stat-ing the next snapshot date
NEXTDAY=`/bin/date +%Y%m%d --date="${OLDESTDATE}+1 day"`
if [ -d $SNAPS/$NEXTDAY ]; then
  echo time /bin/ls -flaR $SNAPS/$NEXTDAY > /dev/null
  time /bin/ls -flaR $SNAPS/$NEXTDAY > /dev/null
fi

echo "Done with snapshot pruning"
