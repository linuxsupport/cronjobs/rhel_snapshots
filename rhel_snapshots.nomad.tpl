job "${PREFIX}_rhel${RELEASE}_snapshots" {
  datacenters = ["*"]

  type = "batch"

  periodic {
    cron = "${SCHEDULE}"
    time_zone = "Europe/Zurich"
    prohibit_overlap = false
  }

  reschedule {
    attempts       = 276
    interval       = "23h"
    unlimited      = false
    delay          = "5m"
    delay_function = "constant"
  }

  task "${PREFIX}_rhel${RELEASE}_snapshots" {
    driver = "docker"

    config {
      image = "${CI_REGISTRY_IMAGE}/rhel${RELEASE}_snapshots:${CI_COMMIT_SHORT_SHA}"
      logging {
        config {
          tag = "${PREFIX}_rhel${RELEASE}_snapshots"
        }
      }
      volumes = [
        "$DATA:/data",
        "$ADVISORIES:/advisories",
        "/etc/nomad/submit_token:/secrets/token",
        "/etc/nomad/ca.pem:/secrets/nomad-ca.pem",
        "/etc/nomad/ssh-id_ed25519:/root/.ssh/id_ed25519",
        "/etc/nomad/linuxci_api_token:/secrets/linuxci_api_token",
        "/etc/nomad/linuxci_pw:/secrets/linuxci_pw",
        "/etc/nomad/rundeck_api_token:/secrets/rundeck_api_token",
        "${MATTERMOST_INTEGRATION_URL_PATH}:/secrets/mattermost_hook_url",
      ]
    }

    env {
      RELEASE = "${RELEASE}"
      PATH_DESTINATION = "$PATH_DESTINATION"
      PRODDATE = "$PRODDATE"
      OLDESTDATE = "$OLDESTDATE"
      EMAIL_FROM = "$EMAIL_FROM"
      EMAIL_ADMIN = "$EMAIL_ADMIN"
      EMAIL_USERS = "$EMAIL_USERS"
      EMAIL_USERS_TEST = "$EMAIL_USERS_TEST"
      NOMAD_ADDR = "$NOMAD_ADDR"
      NOMAD_CACERT = "/secrets/nomad-ca.pem"
      TAG = "${PREFIX}_rhel${RELEASE}_snapshots"
      MATTERMOST_CHANNEL = "$MATTERMOST_CHANNEL"
    }

    resources {
      cpu = 6000 # Mhz
      memory = 1024 # MB
    }

  }
}
