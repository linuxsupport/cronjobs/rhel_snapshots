#!/bin/bash

source /root/common.sh
source /root/common_functions.sh

TMPROOT="$(mktemp -d)"
DNF="dnf --installroot=$TMPROOT --releasever=${RELEASE} --nogpgcheck --disablerepo=*"
TODAY_REPOS="--repofrompath=validation.cern,$DEST/CERN/x86_64 --repofrompath=validation.baseos,$DEST/baseos/x86_64/os --repofrompath=validation.appstream,$DEST/appstream/x86_64/os --excludepkgs pyphonebook"
YESTERDAY_REPOS="--repofrompath=validation.cern,$SNAPS/$YESTERDAY/CERN/x86_64 --repofrompath=validation.baseos,$SNAPS/$YESTERDAY/baseos/x86_64/os --repofrompath=validation.appstream,$SNAPS/$YESTERDAY/appstream/x86_64/os --excludepkgs pyphonebook"
TEMPLATE="email_validation.tpl"
TEMPLATE_WARNING="email_validation_warning.tpl"

run_test () {
  COMMAND=$1
  REPOS=${2:-$TODAY_REPOS}

  $DNF $REPOS $COMMAND &> $TMPROOT/.output
  RET=$?
  grep -q '^Total size:' $TMPROOT/.output; INSTALL=$?
  if [[ $RET -eq 0 ||  # 0 is ok
        ( $RET -eq 1 && $INSTALL -eq 0 )  # 1 may still be ok
      ]]; then
    RET=0
    rm -rf $TMPROOT/.output
  else
    # Log the errors
    cat $TMPROOT/.output
  fi

  return $RET
}

run_debugsolver () {
  COMMAND=$1
  REPOS=${2:-$TODAY_REPOS}

  $DNF $REPOS --assumeno --debugsolver $COMMAND &> $TMPROOT/.output

  # Check the solver results to see if there are problems
  grep -qw '^problem' debugdata/rpms/solver.result
  RES=$?

  # Clean up the debugdata
  rm -rf debugdata/

  # If 'problem' was not found in the results, grep returns 1 and we're happy (so return 0)
  # If grep doesn't return 1, there was a problem (so return 1)
  [[ $RES -ne 1 ]] && return 1 || return 0
}

error () {
  echo "Encountered an error..."
  touch -a "$DESTINATION/.freeze.${RELEASE}all"

  template $TEMPLATE VALIDATION_EMAIL_TEXT="$1" | sed '/^--ERROR--$/Q' > "$TMPROOT/.email"
  tail -n 200 "$TMPROOT/.output" >> "$TMPROOT/.email"
  template $TEMPLATE VALIDATION_EMAIL_TEXT="$1" | sed '1,/^--ERROR--$/d' >> "$TMPROOT/.email"

  cat "$TMPROOT/.email" | swaks --server $MAILMX --to $EMAIL_ADMIN --data -

  exit
}

warning () {
  echo "Encountered an issue..."

  template $TEMPLATE_WARNING VALIDATION_EMAIL_TEXT="$1" | sed '/^--ERROR--$/Q' > "$TMPROOT/.email"
  tail -n 200 "$TMPROOT/.output" >> "$TMPROOT/.email"
  template $TEMPLATE_WARNING VALIDATION_EMAIL_TEXT="$1" | sed '1,/^--ERROR--$/d' >> "$TMPROOT/.email"

  cat "$TMPROOT/.email" | swaks --server $MAILMX --to $EMAIL_ADMIN --data -

  exit
}

echo "Performing basic repository validation tests..."

# install dnf into a temporary installroot
if ! run_test "-y install dnf"; then
  error "bootstap dnf within the installroot"
fi

# dnf was installed in the installroot, let's now try to install cern groups
if ! run_test "--assumeno group install cern-base"; then
  error "install the group 'cern-base'"
fi

if ! run_test "--assumeno group install cern-addons"; then
  error "install the group 'cern-addons'"
fi

if ! run_test "--repofrompath=validation.openafs,$DEST/openafs/x86_64 --assumeno install kmod-openafs"; then
  error "install openafs"
fi

# This group is very finicky because of Qt5 shit which is often broken, so first
# we will try to install it as is, and if that fails we'll try to skip the broken packages.
if ! run_test "--assumeno group install cern-developer-workstation"; then
  # Save the error output
  mv $TMPROOT/.output $TMPROOT/.fail-output
  if ! run_test "--assumeno group install cern-developer-workstation --skip-broken"; then
    error "install the group 'cern-developer-workstation'"
  else
    # Restore the original error output so we can add it to the email
    mv $TMPROOT/.fail-output $TMPROOT/.output
    warning "install the group 'cern-developer-workstation' without --skip-broken"
  fi
fi

# Test the packages in packages_tested.lst
while IFS= read -r RPM; do
  [[ -z "$RPM" ]] && continue

  # Assemble all the repos into a list of --repofrompath parameters
  repos="$(find "$DEST" -path "*/x86_64/*" -not -ipath '*/source/*' -not -ipath '*/debug/*' -not -ipath '*/.clean.*' -name 'repodata' | sed 's#/repodata##' | awk '{printf " --repofrompath=repo" NR "," $0}')"

  if ! run_test "--assumeno install ${RPM}" "$repos"; then
    error "install ${RPM}"
  fi
done <<< "$(sed 's/^\s*//;s/\s*$//;s/\s*#.*$//;/^\s*$/d' /root/packages_tested.lst)"

# Get almost all the group IDs
ALL_GROUPS=$($DNF $REPOS group list -v --hidden | grep '^   ' | sed 's/.*(\(.*\))$/\1/' | grep -v '\(conflicts-\|cern-base\|cern-addons\|cern-developer-workstation\)')

for GROUP in $ALL_GROUPS; do
  # Pretend to install the group, but all we really want is the solver debug data
  # We can't use just --assumeno because it will return non-zero for some groups
  if ! run_debugsolver "group install $GROUP"; then
    warning "group install $GROUP"
  fi
done

# Today's snapshot looks ok, let's test an upgrade from yesterday to today
quickDelete $TMPROOT
mkdir $TMPROOT

echo "Bootstrapping dnf from yesterday's snapshot"
if ! run_test "-y install dnf" "$YESTERDAY_REPOS"; then
  error "bootstap dnf within the installroot with yesterday's snapshot"
fi

echo "Installing cern-developer-workstation from yesterday's snapshot, this might take a while..."
if ! run_test "-y group install cern-developer-workstation" "$YESTERDAY_REPOS"; then
  error "install the group 'cern-developer-workstation' with yesterday's snapshot"
fi

echo "Testing upgrade"
# We have to use --debugsolver for this as well
if ! run_debugsolver "upgrade"; then
  error "perform an upgrade"
fi

echo "All tests passed."
