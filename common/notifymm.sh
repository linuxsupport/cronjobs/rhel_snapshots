#!/bin/bash

source /root/common.sh
source /root/common_functions.sh

TMPDIR=$1
CHANNEL=$2

case $CHANNEL in
  "testing")
    REPO_DEST_PATH="updates/${LONG_RELEASE}/test"
    NEW_RELEASE_FILE=".${RELEASE}-testing-new-release"
    ;;
  "production")
    REPO_DEST_PATH="updates/${LONG_RELEASE}/prod"
    NEW_RELEASE_FILE=".${RELEASE}-new-release"
    ;;
  *)
    echo "CHANNEL not specified, nothing to do."
    exit 0
    ;;
esac

# If we don't have a new release, there's nothing to do
if [[ ! -f "$TMPDIR/${NEW_RELEASE_FILE}" ]]; then
  echo "No new release, nothing to share on Mattermost"
  exit 0
fi

NEW_RELEASE_DETECTED=$(cat "$TMPDIR/${NEW_RELEASE_FILE}")
RELEASE_NAME="${FULL_OS_NAME% $RELEASE} $NEW_RELEASE_DETECTED" # ie. 'Red Hat Enterprise Linux 9.2'

TEXT=$(template chat_newrelease.tpl \
    TODAY_FORMAT="$TODAY_FORMAT" \
    RELEASE_NAME="$RELEASE_NAME" \
    CHANNEL="$CHANNEL" \
    REPO_DEST_PATH="$REPO_DEST_PATH" \
    YEAR_MONTH="$(date +%Y/%m)"
)

# The JSON payload for Mattermost needs to look like this:
# {
#     "channel": "${MATTERMOST_CHANNEL}",
#     "username": "bla",
#     "icon_url": "https://bla",
#     "text": "bla bla\nbla"
# }
DATA=$(jq -cn \
  --arg channel "$MATTERMOST_CHANNEL" \
  --arg username "Lenny the Linux Lookout" \
  --arg icon_url "https://linux.web.cern.ch/img/lenny.jpg" \
  --arg text "$TEXT" \
  '$ARGS.named'
)

echo "Sending message to Mattermost: $DATA"
curl -i -X POST -H 'Content-Type: application/json' \
  -d "$DATA" \
  $(cat /secrets/mattermost_hook_url)
