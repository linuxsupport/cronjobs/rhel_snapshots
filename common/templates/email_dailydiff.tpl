To: $EMAIL_ADMIN
From: $EMAIL_FROM
Reply-To: noreply.$EMAIL_FROM
Return-Path: $EMAIL_ADMIN
Subject: [DIFF]: $RELEASE_ACRONYM - $SHORTLIST [...]

Dear Linux admins,

Here's the current state of the symlinks:

$STATE

Today's $FULL_OS_NAME ($RELEASE_ACRONYM) snapshot ($TODAY) contains the following packages:

--PACKAGES--

---
Best regards,
CERN Linux Droid
(on behalf of the friendly humans of Linux Support)