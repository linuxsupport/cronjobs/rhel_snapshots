#!/bin/bash

SECONDS=0

/root/fixhistory.sh
/root/mksnapshot.sh
/root/tripwire.sh
/root/triggers.sh
/root/validation.sh
/root/movelinks.sh
/root/cleanup.sh

echo "Finished! Elapsed: $(($SECONDS / 3600))hrs $((($SECONDS / 60) % 60))min $(($SECONDS % 60))sec"
