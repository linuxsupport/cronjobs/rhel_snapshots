#!/bin/bash

source /root/common.sh
source /root/common_functions.sh

# Rather than introduce a new variable, test if $DESTINATION is not prod
if [ "$DESTINATION" != "/data/cern/${LONG_RELEASE%${RELEASE}}" ]; then
  echo "On test environment, do not push updates to repo"
  exit 0
fi

TMPDIR=$1
CHANNEL=$2
BRANCH='master'

ADVURL="https://access.redhat.com/errata/"
CVEURL="https://access.redhat.com/security/cve/"

case $CHANNEL in
  "testing")
    REPO_DEST_PATH="updates/${LONG_RELEASE}/test"
    NEW_RELEASE_FILE=".${RELEASE}-testing-new-release"
    ;;
  "production")
    REPO_DEST_PATH="updates/${LONG_RELEASE}/prod"
    NEW_RELEASE_FILE=".${RELEASE}-new-release"
    ;;
  *)
    echo "CHANNEL not specified, no commiting"
    exit 0
    ;;
esac

for ARCH in $SUPPORTED_ARCHES; do
  SQLITE="/usr/bin/sqlite3 file:/advisories/${ADVISORY_DB}/$ARCH/updateinfo.db?mode=ro"

  for REPO in $ALL_REPOS; do
    REPO_DIFF=$TMPDIR/.diff:${REPO}:${ARCH}

    # If there's no diff, there's nothing to do
    [ -s "$REPO_DIFF" ] || continue

    echo "### $REPO $ARCH repository" >> $TMPDIR/.updates
    echo >> $TMPDIR/.updates
    echo "Package | Version | Advisory | Notes" >> $TMPDIR/.updates
    echo "------- | ------- | -------- | -----" >> $TMPDIR/.updates
    for LINE in $(cat $REPO_DIFF); do
      # LOS-483 Do not include i686 packages in the updates
      P_ARCH=$(echo $LINE | awk -F\; '{print $5}')
      [ "$P_ARCH" == "i686" ] && continue

      # Split table variables to ease future css styling if needed, much more readable than one-liners
      RPM=$(echo $LINE | awk -F\; '{print $1}')
      PACKAGE=$(echo $LINE | awk -F\; '{print $2}')
      # Include version and release
      VERSION=$(echo $LINE | awk -F\; '{printf "%s-%s", $3,$4}')

      # Let's get the advisory information
      ADVISORIES=''
      ADVTYPE=''
      CVES=''

      for ADV in $($SQLITE 'select distinct advisory, file from erratapackages where file="'$RPM'" order by id desc'); do
        ADVNAME=$(echo $ADV | awk -F\| '{print $1}')

        CVE_LIST=$($SQLITE 'select group_concat(distinct cve_id) from erratacves where advisory="'$ADVNAME'" order by cve_id')

        TYPE=$($SQLITE 'select type from errata where advisory="'$ADVNAME'"')
        # ADVTYPE should be the highest priority advisory, if there are multiple.
        if [[ "$TYPE" == "Security Advisory" ]] || [[ "$TYPE" == "security" ]]; then
          ADVTYPE='S'
        elif [[ "$TYPE" == "Bug Fix Advisory" ]] || [[ "$TYPE" == "bugfix" ]]; then
          [[ "$ADVTYPE" == "S" ]] && continue
          ADVTYPE='B'
        elif [[ "$TYPE" == "Product Enhancement Advisory" ]] || [[ "$TYPE" == "enhancement" ]]; then
          [[ "$ADVTYPE" == "S" || "$ADVTYPE" == "B" ]] && continue
          ADVTYPE='E'
        fi

        # Make the advisory link to the errata page
        ADVISORIES="$ADVISORIES[$ADVNAME](${ADVURL}${ADVNAME}),"

        [[ ! -z "$CVE_LIST" ]] && CVES="${CVES}${CVE_LIST},"
      done

      # Sort and remove duplicates, and separate with commas.
      ADVISORIES=$(echo $ADVISORIES | tr ',' '\n' | sort -u | xargs | sed 's/ /, /g')

      # Sort, remove duplicates, and format as links.
      # Note the | in the sed. $CVEURL contains /, so we have to use a different separator
      [[ ! -z $CVES ]] && CVES=$(echo $CVES | sed 's/,$//' | tr ',' '\n' | sort -u | sed "s|\(.*\)|[\1]($CVEURL\1)|" | sed ':a; N; $!ba; s/\n/, /g')

      if [[ ! -z $ADVTYPE ]]; then
        # text type might be just "security" so we specify the text to insert
        if [[ "$ADVTYPE" == "S" ]]; then
          ADVTYPE_HTML='<div class="adv_s">Security Advisory</div>'
        elif [[ "$ADVTYPE" == "B" ]]; then
          ADVTYPE_HTML='<div class="adv_b">Bug Fix Advisory</div>'
        elif [[ "$ADVTYPE" == "E" ]]; then
          ADVTYPE_HTML='<div class="adv_e">Product Enhancement Advisory</div>'
        else
          ADVTYPE_HTML='<div class="adv_e">[?]</div>'
        fi

        [[ ! -z $CVES ]] && ADVTYPE_HTML="${ADVTYPE_HTML} (${CVES})"

        echo "$PACKAGE | $VERSION | $ADVISORIES | $ADVTYPE_HTML" >> $TMPDIR/.updates
      else
        echo "$PACKAGE | $VERSION | |" >> $TMPDIR/.updates
      fi

    done
    echo >> $TMPDIR/.updates

  done
done

if [ ! -s "$TMPDIR/.updates" ]; then
  echo "There are no updates, no need to commit a file"
  exit 0
fi

# Add the header to the beginning of the file
# Never use first level header or mkdocs will not build table of contents with multiple h1 in the same page
sed -i "1i## $TODAY_FORMAT\n" $TMPDIR/.updates

# We have a new release, so let's create a new news entry for it
if [[ -f "$TMPDIR/${NEW_RELEASE_FILE}" ]]; then
  NEW_RELEASE_DETECTED=$(cat "$TMPDIR/${NEW_RELEASE_FILE}")
  RELEASE_NAME="${FULL_OS_NAME% $RELEASE} $NEW_RELEASE_DETECTED" # ie. 'Red Hat Enterprise Linux 9.2'
  template news_newrelease.tpl \
    TODAY_FORMAT="$TODAY_FORMAT" \
    RELEASE_NAME="$RELEASE_NAME" \
    CHANNEL="$CHANNEL" \
    REPO_DEST_PATH="$REPO_DEST_PATH" \
    NEW_RELEASE_DETECTED="$NEW_RELEASE_DETECTED" \
    YEAR_MONTH="$(date +%Y/%m)" > $TMPDIR/.news
fi

# Let's save the updates, just in case something goes wrong while committing them
cp $TMPDIR/.updates $FINALDEST/.updates-$CHANNEL
[[ -f "$TMPDIR/.news" ]] && cp $TMPDIR/.news $FINALDEST/.news-$CHANNEL

# Through volumes, nomad will have the required ssh key for git interaction
git config --global user.name "CERN Linux Droid"
git config --global user.email "linux.ci@cern.ch"

# We use ssh for git, therefore we add gitlab to known_hosts for non interactive git commands
ssh-keyscan -t rsa -p 7999 gitlab.cern.ch >> ~/.ssh/known_hosts

# We use ssh, nomad mounts the ssh key secret (coming from teigi) for this to work
rm -rf /documentation
git clone --depth 1 ssh://git@gitlab.cern.ch:7999/linuxsupport/websites/linux.cern.ch.git /documentation
if [[ $? -ne 0 ]]; then
  echo "Failed to clone the documentation repository, skipping commit."
  echo "Generated files will be available in $FINALDEST"
  exit
fi

cd /documentation
git checkout $BRANCH

mkdir -p /documentation/docs/$REPO_DEST_PATH
cat $TMPDIR/.updates >> /documentation/docs/$REPO_DEST_PATH/$TODAY.md
git add /documentation/docs/$REPO_DEST_PATH/$TODAY.md

if [[ -f $TMPDIR/.news ]]; then
  cat $TMPDIR/.news >> /documentation/docs/news/$TODAY.md
  git add /documentation/docs/news/$TODAY.md
fi

git commit -m "${LONG_RELEASE^^} $CHANNEL updates $TODAY_FORMAT"
if [[ $? -ne 0 ]]; then
  echo "Failed to commit to documentation repository."
  echo "Generated files will be available in $FINALDEST"
  exit
fi

# Disable manual edit of commit message so it automerges
export GIT_MERGE_AUTOEDIT=no
# Merge made by the 'recursive' strategy. It does not conflict with other jobs
git pull --rebase
git push origin $BRANCH
if [[ $? -ne 0 ]]; then
  echo "Failed to push changes to documentation repository"
  echo "Generated files will be available in $FINALDEST"
  exit
fi

rm $TMPDIR/.updates
rm -f $TMPDIR/.news
rm -vf $FINALDEST/.updates-$CHANNEL $FINALDEST/.news-$CHANNEL
