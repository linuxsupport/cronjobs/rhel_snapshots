#!/bin/bash

source /root/common.sh

REPO_REGEX="$1"
TAG_REGEX="$2"
REGEN_JOB="prod_koji-regenrepo"

PAYLOAD="{"
[[ -n "$REPO_REGEX" ]] && PAYLOAD="$PAYLOAD \"REPO_REGEX\": \"${REPO_REGEX}\","
[[ -n "$TAG_REGEX"  ]] && PAYLOAD="$PAYLOAD \"TAG_REGEX\": \"${TAG_REGEX}\","
PAYLOAD="$PAYLOAD \"FORCE\": \"false\" }"
DATA="{ \"Meta\": {\"PARENT_JOB\": \"$NOMAD_TASK_NAME\"}, \"Payload\": \"`echo -n "$PAYLOAD" | base64 -w 0`\"}"

echo "Triggering Nomad job with payload: ${PAYLOAD}"
curl -H "X-Nomad-Token: `cat /secrets/token`" \
    -X POST \
    --cacert /secrets/nomad-ca.pem \
    --data "$DATA" \
    ${NOMAD_ADDR}/v1/job/${REGEN_JOB}/dispatch

echo
